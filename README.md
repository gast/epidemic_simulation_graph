# epidemic_simulation_graph

This repository contains a (small) code to simulate an SIR model on a graph. 

At each time step, we pick a node. If this node is infected:
- it infects each of its susceptible neighbors with probability $p$
- it then becomes "recovered" with probability $r$

$p$ is such that the number of infected people per person is R0. 